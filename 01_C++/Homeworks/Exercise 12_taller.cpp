/* 12. Escriba un programa para calcular el tiempo transcurrido, en minutos, necesario para hacer un viaje.  
La ecuaci�n es tiempo transcurrido = distancia total/velocidad promedio. Suponga que la distancia est� en
 kil�metros y la velocidad en kil�metros/hora. */
 
#include <iostream>
#include <math.h>

using namespace std;


 	float tiempo=0.0;           
 	float distancia=0.0;               
 	float velocidad = 0;			

 	
// FUNCTION DECLARATION

	void Run();
	void CollectData();
	void Calculate();
	void ShowResults();
 	float CalculationTiempoTranscurrido(float distancia,float velocidad );

	

 	int  main(){
 	
	
	Run();
 	return 0;
 	
 }
 
// FUNCTION DEFINITION


void Run(){
	CollectData();
	Calculate();
	ShowResults();
}



	//Data entry in the terminal
void CollectData(){	

 	cout<<"Insert data\r\n";
 	
 	cout<<"Ingrese la distancia recorrida  :\r\n"; cin>>distancia;
 	
 	cout<<"ingrese la velocidad promedio :\r\n"; cin>>velocidad;
	
 	
 	
}


	//Operation of  the variables
	void Calculate(){
	 tiempo=CalculationTiempoTranscurrido(velocidad,distancia);
	 
}


	//Show the calculation of the terminal
	void ShowResults(){
 	cout<<"Show result\r\n";
 	cout<<"el tiempo transcurrido en minutos es  : "<<tiempo<<"\r\n";
 
	 }


	float CalculationTiempoTranscurrido(float distancia,float velocidad ){
	
	return  60*distancia/velocidad;
}


