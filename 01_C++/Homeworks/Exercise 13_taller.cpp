// Exercise 13

#include <iostream>
#include <math.h>

using namespace std;

#define PI 	3.1416	
 
int main(){
	
	float height         = 0.0; //Altura del tonel
	float largerDiameter = 0.0; 
	float minorDiamater  = 0.0; 
	float volume         = 0.0; 
	

	cout<<"Insert Data\r\n";
	
	cout <<"Digite la altura: "; cin>>height ;                                    
	cout<<"\r\nDigite el diametro  mayor en cm: "; cin>>largerDiameter ;                              
	cout<<"\r\nDigite el diametro menor en cm: "; cin>>minorDiamater ;                               
	
	
	volume = (PI/12)*(height)*(2*(pow(largerDiameter,2))+pow(minorDiamater,2)) ; //Formula para hallar el volumen del tonel con la altura y los diametros de sus extremos y  del centro
	
	
	cout<<"\r\n Show results\r\n";
	cout<<"El volumen del tonel es : "<<volume<<"cm� " ;  

	return 0 ;
	
}

