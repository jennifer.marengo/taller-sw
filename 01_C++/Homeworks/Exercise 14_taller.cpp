/* 14. Modifique el programa anterior para que, suponiendo que las medidas de entrada son dadas en cent�metros, 
el resultado lo muestre en: litros, cent�metros c�bicos y metros c�bicos. Recuerde que 1 litro es equivalente a 
un dec�metro c�bico. Indique siempre la unidad de medida empleada. */

#include <iostream>
#include <math.h>

using namespace std;


 	float volumen=0.0;
	float volumen_litros=0.0;
	float volumen_metros=0.0;          
 	float longitud=0.0;               
 	float diametro = 0;	
	float diametroM=0.0;
	float altura=0.0;
			
// Function declaration

	void Run();
	void CollectData();
	void Calculate();
	void ShowResults();
 	float CalculationVolumenLitros(float longitud,float altura );
 	float CalculationVolumen(float longitud,float altura );
 	float CalculationVolumenMetros(float longitud,float altura );
 	float CalculationAltura( float diametro,float diametroM );
 	

 	int  main(){
 	
	
	Run();
	
 	return 0;
 	
 }

//Function declaration

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

// Data entry terminal

void CollectData(){	
 	cout<<"Insert data\r\n";
 	
 	cout<<"Ingrese la longitud del tonel en centimetros: \r\n"; cin>>longitud;
 	cout<<"ingrese la altura del tonel en centimetros: \r\n"; cin>>altura;
 	cout<<"ingrese el diametro de las bases en centimetros: \r\n"; cin>>diametro;
 	cout<<"ingrese el diametro mayor en centimetros: \r\n"; cin>>diametroM;
 	
	
 	
 	
}

	//Operation of  the variables
	
	void Calculate(){
	 volumen=CalculationVolumen( longitud,altura );
	 volumen_litros=CalculationVolumenLitros( longitud,altura );
	 volumen_metros=CalculationVolumenMetros( longitud,altura );
	 altura=CalculationAltura( diametro, diametroM );
}

	//Show the calculation of the terminal
	
	void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"El volumen del tonel es   :    "<<volumen_metros<< " metros cubicos"<<"\r\n";
 	cout<<"El volumen del tonel es   :    "<<volumen<< " centrimetros cubicos"<<"\r\n";
 	cout<<"El volumen del tonel es   :   "<<volumen_litros<< " litros"<<"\r\n";
 
	 }


	float CalculationAltura(float diametro,float diametroM){
	
	
	return diametro/2 + diametroM/3-diametro/3;
	
	
}

	float CalculationVolumen(float longitud,float altura){
	
	return  M_PI*longitud*altura*altura;
}



float CalculationVolumenLitros(float longitud,float altura){
	
	return  M_PI*longitud*altura*altura*pow(10,-5);
	
}



float CalculationVolumenMetros(float longitud,float altura){
	
	return  M_PI*longitud*altura*altura*pow(10,-6);

}

