/* 15. Escriba un programa en C++ para calcular el valor de la pendiente de una l�nea que conecta dos puntos (x1,y1) y (x2,y2).
 		  La pendiente est� dada por la ecuaci�n (y2-y1)/(x2-x1). Haga que el programa tambi�n calcule el punto medio de la
		  l�nea que une los dos puntos, el cual viene dado por (x1+x2)/2,(y1+y2)/2. �Cu�l es el resultado que devuelve el programa
		  para los puntos (3,7) y (8,12)?
 */

#include <iostream>
using namespace std;

float inputAbscissaX1 = 0.0;
float inputOrdinateY1 = 0.0;
float inputAbscissaX2 = 0.0;
float inputOrdinateY2 = 0.0;

float SlopeOfTheLine = 0.0;
float MiddlePointX = 0.0;
float MiddlePointY = 0.0;

//Function declaration

void Run();
void CollectData();
void Calculate();
void ShowResults();
void Opcion(float abscissaX1,float ordinateY1,float abscissaX2,float ordinateY2); //
float CalculateToSlope(float abscissaX1,float ordinateY1,float abscissaX2,float ordinateY2);
float CalculateTheMidpoint(float coordinate1,float coordinate2);


int main(){
	Run();

	return 0;
}

// Function definition

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

void CollectData(){
	cout<<"Insert data\r\n";
	cout<<"Calculation in a course\r\nShape coordinates\r\n(x1,y1) Y (x2,y2)\r\n";
	cout<<"\tType the coordinate x1: ";
	cin>>inputAbscissaX1;
	cout<<"\tType the coordinate y1: ";
	cin>>inputOrdinateY1;
	cout<<"\tType the coordinate x2: ";
	cin>>inputAbscissaX2;
	cout<<"\tType the coordinate y2: ";
	cin>>inputOrdinateY2;
}


void Calculate(){
	Opcion(inputAbscissaX1,inputOrdinateY1,inputAbscissaX2,inputOrdinateY2);
	SlopeOfTheLine = CalculateToSlope(inputAbscissaX1,inputOrdinateY1,inputAbscissaX2,inputOrdinateY2);
	MiddlePointX = CalculateTheMidpoint(inputAbscissaX1,inputAbscissaX2);
	MiddlePointY = CalculateTheMidpoint(inputOrdinateY1,inputOrdinateY2);
}
//=====================================================================================================

void ShowResults(){
	cout<<"Show result\r\n";
	cout<<"\tThe slope of the line is: "<<SlopeOfTheLine<<"\r\n";
	cout<<"\tThe coordinates of the midpoint of the two coordinates are: ("<<MiddlePointX<<","<<MiddlePointY<<")\r\n";
}


void Opcion(float abscissaX1,float ordinateY1,float abscissaX2,float ordinateY2){
	int opcion = 0;

	cout<<"\r\nThe coordinates are: "<<"\r\n";
	cout<<"\r\n("<<abscissaX1<<","<<ordinateY1<<")"<<" and ("<<abscissaX2<<","<<ordinateY2<<")"<<"\r\n";
	cout<<"\r\nIt's correct?: "<<"\r\n";
	cout<<"Press 1 for yes and 2 for no: ";
	cin>>opcion;
	switch(opcion){
		case 1:
			cout<<"Verified"<<"\r\n";
			break;
		case 2:
			cout<<"\r\nIt's incorrect"<<"\r\n";
			CollectData();
			break;
		default:
			cout<<"The command isn't correct"<<"\r\n";
			Opcion(abscissaX1,ordinateY1,abscissaX2,ordinateY2);
	}

}

float CalculateToSlope(float abscissaX1,float ordinateY1,float abscissaX2,float ordinateY2){
	return (ordinateY2-ordinateY1)/(abscissaX2-abscissaX1);
}

float CalculateTheMidpoint(float coordinate1,float coordinate2){
	float midpoint = 0.0;

	midpoint = (coordinate1 + coordinate2)/2.0;

	return midpoint;
}




