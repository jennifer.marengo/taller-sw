// Exercise 17 

#include <iostream>
#include <math.h>
using namespace std;

//Define
#define PI 3.1416

//Global variables
float terminalMinorRadius = 0.0;
float terminalMajorRadius = 0.0;

float area = 0.0;
float average = 0.0;

//Function declaration
void Run();
void CollectData();
void Calculate();
void ShowResults();

float CalculateArea(float MinorRadius, float MajorRadius);
float CalculateAverage(float MinorRadius, float MajorRadius);

//Main

int main(){
	Run();
	return 0;
}   

// Function definition

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

void CollectData(){
	cout<<"Insert data\r\n";
	cout<<"\tWrite the minor radius: "; cin>>terminalMinorRadius;
	cout<<"\tWrite the major radius: "; cin>>terminalMajorRadius;
}

void Calculate(){
	area = CalculateArea(terminalMinorRadius, terminalMajorRadius);
	
}
void ShowResults(){
	cout<<"\r\nShow results\r\n";
	cout<<"\tThe area is: "<<area<<"\r\n";
	cout<<"\tThe average is: "<<average<<"\r\n";
}

float CalculateArea(float MinorRadius, float MajorRadius){
	return PI * MinorRadius * MajorRadius;
	}
float CalculateAverage(float MinorRadius, float MajorRadius){
	
	return 2.0*PI*sqrt((pow(MinorRadius,2) + pow(MajorRadius,2))/2.0);  //perimeter
}


