/* 20.- Ingresar los lados de un triangulo y el �ngulo que forman,
 e imprima el  valor del tercer lado,  los otros dos �ngulos y 
 el �rea del tri�ngulo.
*/
#include <iostream>
#include <math.h>

using namespace std;

int main(){
	

	double ladoA, ladoB, anguloFormado;
	double ladoC;
	double area,rad;
	double perimetro, semiPerimetro;
	
	
	cout<<"Digite lado a: ";
	cin>>ladoA;
	cout<<"Digite lado b: ";
	cin>>ladoB;
	cout<<"Digite el angulo formado: ";
	cin>>anguloFormado;

	rad = anguloFormado*M_PI/180;
	ladoC = sqrt(pow(ladoA,2)+pow(ladoB,2)-(2*ladoA*ladoB*cos(rad)));
	perimetro = ladoA+ladoB+ladoC;
	semiPerimetro = perimetro/2;
	
	area = sqrt((semiPerimetro)*(semiPerimetro-ladoA)*(semiPerimetro-ladoB)*(semiPerimetro-ladoC));
	
	cout<<"\n\rLadoC: "<<ladoC<<endl;
	cout<<"\n\rArea: "<<area<<endl;
	//cout<<"\n\rArea: "<<area<<endl;
	
	
	return 0;
	
}

