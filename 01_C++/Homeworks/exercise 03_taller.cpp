/* 3. Queremos conocer los datos estad�sticos de una asignatura, por lo tanto, necesitamos un algoritmo que
 lea el n�mero de desaprobados, aprobados, notables y sobresalientes de una asignatura, y nos devuelva:
 a. El tanto por ciento de alumnos que han superado la asignatura.
 b. El tanto por ciento de desaprobados, aprobados, notables y sobresalientes de la asignatura. */

#include<iostream>

using namespace std;

int main (){

	int  approv= 0;        //Approv people  11-14
	int  disapp= 0;        //Disapproved people  <=10
	int   sign= 0;         //Significant people   15-17
	int   outSt= 0;        //Outstanding people    18-20
	int   total= 0;        //Amount of people
	int   tota2=  0;       //Amount of not disapproved people
	float percentAp =0.0;  //Approved percentage
	float percentDis=0.0;  //Disapproved percentage
	float percentSign=0.0; //Significant percentage
	float percentOut=0.0;  //Outstanding percentage
	float percentNot=0.0;  //Not disapproved people percentage


	cout<<"Write the amount of approved people\r\n"; cin>>approv;
	cout<<"Write the amount of disapproved people\r\n"; cin>>disapp;
	cout<<"Write in order the amout of significant and outstanding people\r\n"; cin>>sign;
	cout<<"\r";
	cin>>outSt;
	total= approv+disapp+sign+outSt;
	tota2= approv+sign+outSt;
	percentAp=(approv*100)/total;
    percentDis=(disapp*100)/total;
    percentSign=(sign*100)/total;
    percentOut=(outSt*100)/total;
    percentNot=(tota2*100)/total;

	//Display of results
	cout<<"\r\nThe percentage of not disapproved people is: \r\n"<<percentNot;
	cout<<"\r\nThe percentage of Disapproved people: \r\n"<<percentDis;
	cout<<"\r\nThe percentage of Approved people: \r\n"<<percentAp;
	cout<<"\r\nThe percentage of Significant people: \r\n"<<percentSign;
	cout<<"\r\nThe percentage of Outstanding people: \r\n"<<percentOut;

	return 0;
}

