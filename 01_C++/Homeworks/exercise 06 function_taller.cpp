// 6. Desglosar cierta cantidad de segundos a su equivalente en d�as, horas, minutos y segundos.

#include<iostream>
#include<conio.h>

using namespace std;

void time(int,int&,int&,int&);

int main(){
	int totalSec = 0;
	int hours = 0;
	int min = 0;
	int sec = 0;

	//Display first part
	cout<<"Write the quantity of seconds down: "; cin>>totalSec;

    //Display function
	time(totalSec,hours,min,sec);

    //Display of algorithm
    cout<<"\nHoras: "<<hours<<endl;
	cout<<"Minutos: "<<min<<endl;
	cout<<"Segundos: "<<sec<<endl;

	getch();
	return 0;
}

// This function will do all the process
void time(int totalSec,int& hours,int& min,int& sec){
	hours = totalSec/3600;
	totalSec %= 3600;
	min = totalSec/60;
	sec = totalSec%60;
}

