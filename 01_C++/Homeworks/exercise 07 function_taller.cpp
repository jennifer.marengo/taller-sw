// 7. formula: F = G*masa1*masa2 / distancia^2 y G = 6.673 * 10-8 cm3/g.seg2

#include<iostream>
#include<conio.h>
using namespace std;
float strength(float distance, float first_mass, float second_mass) ;
int main(){
	double G = 6.673 ;
	float distance = 0.0;
	float first_mass = 0.0;
	float second_mass = 0.0;
	float n;

	//Display of algorithm

	cout<<"Write the distance down: "; cin>>distance;

	cout<<"\nWrite the first mass down: "; cin>>first_mass;

	cout<<"\nWrite the second mass down: "; cin>>second_mass;

	//Display function

	n=strength(distance,first_mass,second_mass);


	//Final part algorithm
	cout<<"\nThe attraction force is: ";
	cout<<n<<" Newtons";

	getch();
	return 0;
}

float strength(float distance,float first_mass, float second_mass){
	float G = 6.673;
	float str = 0.0;
	str=(G*first_mass*second_mass)/distance;

	return str;
}
